from django.apps import AppConfig


class myAppNameConfig(AppConfig):
    name = 'help'
    verbose_name = 'Help'