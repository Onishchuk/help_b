
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

from apps.program.models import Program
from apps.user.models import Profile

User = get_user_model()


class Money(models.Model):

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    product = models.ForeignKey(Program, null=True, blank=True, on_delete=models.CASCADE)

    amount = models.FloatField(default=0)

    def __str__(self):
        return unicode(self.product)

    class Meta:
        verbose_name = 'Деньги'
        verbose_name_plural = 'Деньги'
