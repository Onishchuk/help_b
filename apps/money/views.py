import json

from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpResponse, Http404
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from apps.liqpay import LiqPay
from apps.money.models import Money
from apps.money.serializers import MoneySerializer
from apps.program.models import HelpProgram, Program
from apps.user.models import Profile
from django.shortcuts import get_object_or_404
from rest_framework import exceptions
from apps.user.serializers import UserSerializer, ProfilePutSerializer, UserProfileSmallSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny

User = get_user_model()


class LiqPayViews(APIView):
    # permission_classes = [permissions.IsAuthenticated, ]
    # permission_classes = (permissions.IsAuthenticated,)
    # authentication_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return Money.objects.get(pk=pk)
        except Money.DoesNotExist:
            raise Http404

    def post(self, request, format=None):

        serializer = MoneySerializer(data=request.data)

        if serializer.is_valid():
            serializer.user = request.user
            serializer.save()
            print(serializer.user)

        liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)

        params = {
            'product_name': str(request.data.get('product')),
            'action': 'pay',
            'amount': str(request.data.get('amount')),
            'currency': 'UAH',
            'description': str(serializer.user),
            'order_id': '57',
            'version': '3',
            'sandbox': 1,  # sandbox mode, set to 1 to enable it
            'result_url': 'http://127.0.0.1:8000/api/v1/money/liq-pay/cols/',
            'server_url': 'http://127.0.0.1:8000/api/v1/money/liq-pay/cols/',  # url to callback view
        }
        signature = liqpay.cnb_signature(params)
        data = liqpay.cnb_data(params)

        return Response({'signature': signature, 'data': data}, status=status.HTTP_201_CREATED)


@method_decorator(csrf_exempt, name='dispatch')
class PayCallbackView(APIView):

    def post(self, request, *args, **kwargs):
        print('Col')
        liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQPAY_PRIVATE_KEY + data + settings.LIQPAY_PRIVATE_KEY)
        if sign == signature:
            print('callback is valid')
            print(liqpay.decode_data_from_str(data))
            rez = liqpay.decode_data_from_str(data)

            print(rez.get('product_name'))
            print(rez.get('amount'))
            print(rez.get('description'))

            # if str(rez.get('description')) != 'None' and str(rez.get('description')) != 'AnonymousUser':
            #     print('One')
            #     print(rez.get('description'))
            #     user = get_object_or_404(User, name=rez.get('description'))
            #     new_help = HelpProgram(
            #         user=get_object_or_404(Profile, user=user),
            #         money=rez.get('amount'),
            #         program = get_object_or_404(Program, pk=int(rez.get('product_name')))
            #     )
            #     new_help.save()
            #
            # else:
            #     print('Two')
            new_help = HelpProgram(
                money=str(rez.get('amount')),
                program = get_object_or_404(Program, pk=int(rez.get('product_name')))
            )
            new_help.save()

        # response = liqpay.decode_data_from_str(data)
        # print('callback data', response)
        return redirect('http://localhost:4200/')