
from django.contrib import admin

from apps.money.models import Money


class MoneyAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'product', 'amount',)


admin.site.register(Money, MoneyAdmin)
