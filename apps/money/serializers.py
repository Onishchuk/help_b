
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from apps.money.models import Money
from apps.program.models import Category, Program, HelpProgram
from apps.user.serializers import UserProfileSmallSerializer


class MoneySerializer(serializers.ModelSerializer):
    class Meta:
        model = Money
        fields = '__all__'

    def save(self):
        user = CurrentUserDefault()  # <= magic!
