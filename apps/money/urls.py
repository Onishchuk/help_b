
from django.urls import path

from apps.money.views import *
from django.conf.urls import url


urlpatterns = [

    path('liq-pay/', LiqPayViews.as_view()),
    path('liq-pay/cols/', PayCallbackView.as_view()),

]