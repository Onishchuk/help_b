
from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.contact.models import Contact
from apps.user.models import Profile

User = get_user_model()


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'
