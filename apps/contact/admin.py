
from django.contrib import admin

from apps.contact.models import Contact
from apps.user.models import *


class ContactAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'topic',)


admin.site.register(Contact, ContactAdmin)
