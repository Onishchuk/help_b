from django.db import models
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from pytz import unicode


User = get_user_model()


class Contact(models.Model):

    name = models.CharField(max_length=200)

    email = models.EmailField()

    topic = models.CharField(max_length=250)

    text = models.TextField()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name = 'Сообщения'
        verbose_name_plural = 'Сообщения'

