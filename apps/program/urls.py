
from django.urls import path

from apps.program.views import GetListAllProgram, ProgramView, GetListHelpProgramView, GetListUserHelpProgramView

urlpatterns = [

    path('all/', GetListAllProgram.as_view()),
    path('info/<int:pk>/', ProgramView.as_view()),
    path('help/<int:program_pk>/', GetListHelpProgramView.as_view()),
    path('help/user/<int:user_pk>/', GetListUserHelpProgramView.as_view()),

]