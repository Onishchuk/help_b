
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

from apps.user.models import Profile

User = get_user_model()


class Category(models.Model):

    name = models.CharField(max_length=250)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = 'Категории'
        verbose_name_plural = 'Категории'


class Program(models.Model):

    logo = models.ImageField('img', null=True, blank=True, upload_to='image')

    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    name = models.CharField(max_length=250)

    small_description = models.CharField(max_length=250)

    text = models.TextField()

    part_money = models.FloatField(default=0)

    all_money = models.FloatField(default=0)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = 'Програми'
        verbose_name_plural = 'Програми'


class HelpProgram(models.Model):

    user = models.ForeignKey(Profile, null=True, blank=True, on_delete=models.CASCADE)

    money = models.CharField(max_length=250)

    program = models.ForeignKey(Program, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = 'Помощь для проекта'
        verbose_name_plural = 'Помощь для проекта'


