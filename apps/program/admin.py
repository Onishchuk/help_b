
from django.contrib import admin

from apps.program.models import Category, Program, HelpProgram


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


class ProgramAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'name', 'part_money', 'all_money',)


class HelpProgramAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'money', 'program',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Program, ProgramAdmin)
admin.site.register(HelpProgram, HelpProgramAdmin)
