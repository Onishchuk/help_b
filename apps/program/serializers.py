
from rest_framework import serializers

from apps.program.models import Category, Program, HelpProgram
from apps.user.serializers import UserProfileSmallSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class ProgramSerializer(serializers.ModelSerializer):

    category = CategorySerializer(read_only=True, many=False)

    class Meta:
        model = Program
        fields = '__all__'


class HelpProgramSerializer(serializers.ModelSerializer):
    user = UserProfileSmallSerializer(read_only=True, many=False)

    class Meta:
        model = HelpProgram
        fields = '__all__'
