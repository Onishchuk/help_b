
from django.contrib.auth import get_user_model
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.pagination import *
from rest_framework.views import APIView

from apps.program.models import Program, HelpProgram
from apps.program.pagination import ProgramPagination
from apps.program.serializers import ProgramSerializer, HelpProgramSerializer

User = get_user_model()


class GetListAllProgram(generics.ListAPIView):
    serializer_class = ProgramSerializer
    pagination_class = ProgramPagination

    def get_queryset(self):
        return Program.objects.all().order_by('-id')


class ProgramView(APIView):

    def get_object(self, pk):
        try:
            return Program.objects.get(pk=pk)
        except Program.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ProgramSerializer(snippet)
        return Response(serializer.data)


class GetListHelpProgramView(generics.ListAPIView):
    serializer_class = HelpProgramSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        program_pk = self.kwargs['program_pk']
        return HelpProgram.objects.filter(program=get_object_or_404(Program, pk=program_pk))


class GetListUserHelpProgramView(generics.ListAPIView):
    serializer_class = HelpProgramSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        user_pk = self.kwargs['user_pk']
        return HelpProgram.objects.filter(user=get_object_or_404(User, pk=user_pk))
