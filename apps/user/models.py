from django.db import models
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from pytz import unicode


User = get_user_model()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    first_name = models.CharField(max_length=250, null=True, blank=True)

    last_name = models.CharField(max_length=250, null=True, blank=True)

    location = models.CharField(max_length=200, null=True, blank=True)

    status = models.CharField(max_length=200, null=True, blank=True)

    phone = models.CharField(max_length=200, null=True, blank=True)

    email = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.user

    class Meta:
        verbose_name = '1. Профиль'
        verbose_name_plural = '1. Профиль'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
