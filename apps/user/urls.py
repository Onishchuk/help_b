
from django.urls import path

from apps.contact.views import ProfileViews, GetUser
from apps.user.views import *

urlpatterns = [

    path('<int:pk>/', GetUser.as_view()),
    path('profile/<int:pk>/', ProfileViews.as_view()),

]