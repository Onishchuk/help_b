
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

User = get_user_model()


class Home(models.Model):

    name = models.CharField(max_length=250)

    text = models.TextField()

    location = models.CharField(max_length=200, null=True, blank=True)

    email = models.CharField(max_length=200, null=True, blank=True)

    phone = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = 'Информация'
        verbose_name_plural = 'Информация'
