
from django.urls import path

from apps.galery.views import AlbumView, GetListAllGalery, ListPhotoView

urlpatterns = [

    path('all/', GetListAllGalery.as_view()),
    path('info/<int:pk>/', AlbumView.as_view()),

    path('photo/<int:category_pk>/', ListPhotoView.as_view()),

]