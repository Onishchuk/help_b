
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

User = get_user_model()


class Album(models.Model):

    image = models.ImageField('img', null=True, blank=True, upload_to='image')

    name = models.CharField(max_length=250)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = 'Альбом'
        verbose_name_plural = 'Альбом'


class Photo(models.Model):

    album = models.ForeignKey(Album, on_delete=models.CASCADE)

    image = models.ImageField('img', null=True, blank=True, upload_to='image')

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = 'Фото в альбомі'
        verbose_name_plural = 'Фото в альбомі'
