
from django.contrib import admin

from apps.galery.models import Album, Photo


class AlbumAdmin(admin.ModelAdmin):
    list_display = ('id', 'image', 'name',)


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'album', 'image',)


admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)
