
from rest_framework import serializers

from apps.galery.models import Album, Photo
from apps.program.models import Category, Program


class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = '__all__'


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = '__all__'
