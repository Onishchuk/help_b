
from django.contrib.auth import get_user_model
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.pagination import *
from rest_framework.views import APIView

from apps.galery.models import Album, Photo
from apps.galery.pagination import GaleryPagination, PhotoPagination
from apps.galery.serializers import AlbumSerializer, PhotoSerializer
from apps.program.models import Program
from apps.program.serializers import ProgramSerializer

User = get_user_model()


class GetListAllGalery(generics.ListAPIView):
    serializer_class = AlbumSerializer
    pagination_class = GaleryPagination

    def get_queryset(self):
        return Album.objects.all().order_by('-id')


class AlbumView(APIView):

    def get_object(self, pk):
        try:
            return Album.objects.get(pk=pk)
        except Album.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AlbumSerializer(snippet)
        return Response(serializer.data)


class ListPhotoView(generics.ListAPIView):
    serializer_class = PhotoSerializer
    pagination_class = PhotoPagination


    def get_queryset(self):
        album_pk = self.kwargs['category_pk']
        return Photo.objects.filter(album=get_object_or_404(Album, pk=album_pk))