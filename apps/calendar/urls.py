
from django.urls import path

from apps.calendar.views import GetListAllCalendar, CalendarView
from apps.program.views import GetListAllProgram, ProgramView, GetListHelpProgramView

urlpatterns = [

    path('all/', GetListAllCalendar.as_view()),
    path('info/<int:pk>/', CalendarView.as_view()),

]