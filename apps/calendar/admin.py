
from django.contrib import admin

from apps.calendar.models import Calendar


class CalendarAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'location', 'email', 'phone', 'date_start', 'date_end', 'visible',)


admin.site.register(Calendar, CalendarAdmin)
