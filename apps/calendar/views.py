
from django.contrib.auth import get_user_model
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.pagination import *
from rest_framework.views import APIView

from apps.calendar.models import Calendar
from apps.calendar.serializers import CalendarSerializer
from apps.program.models import Program, HelpProgram
from apps.program.serializers import ProgramSerializer, HelpProgramSerializer

User = get_user_model()


class GetListAllCalendar(generics.ListAPIView):
    serializer_class = CalendarSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self):
        return Calendar.objects.filter(visible=True).order_by('-id')


class CalendarView(APIView):

    def get_object(self, pk):
        try:
            return Calendar.objects.get(pk=pk)
        except Calendar.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CalendarSerializer(snippet)
        return Response(serializer.data)
