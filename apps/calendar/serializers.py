
from rest_framework import serializers

from apps.calendar.models import Calendar
from apps.program.models import Category, Program, HelpProgram


class CalendarSerializer(serializers.ModelSerializer):

    date_start = serializers.DateTimeField(format="%Y-%m-%d %H:%M", required=False, read_only=True)

    date_end = serializers.DateTimeField(format="%Y-%m-%d %H:%M", required=False, read_only=True)

    class Meta:
        model = Calendar
        fields = '__all__'
