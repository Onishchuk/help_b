
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode

User = get_user_model()


class Calendar(models.Model):

    logo = models.ImageField('img', null=True, blank=True, upload_to='image')

    name = models.CharField(max_length=250)

    small_description = models.CharField(max_length=250)

    text = models.TextField()

    location = models.CharField(max_length=200, null=True, blank=True)

    email = models.CharField(max_length=200, null=True, blank=True)

    phone = models.CharField(max_length=200, null=True, blank=True)

    date_start = models.DateTimeField(null=True, blank=True)

    date_end = models.DateTimeField(null=True, blank=True)

    visible = models.BooleanField(default=False)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = 'Календар'
        verbose_name_plural = 'Календар'
